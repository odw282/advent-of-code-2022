import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { countCalories, countCaloriesTopThree  } from "./index";
const inventory = readFileIntoArray(path.join(__dirname, "./input.txt"));

describe("--- Day 1: Calorie Counting ---", () => {
  test("Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?", () => {
    const answer = countCalories(inventory);
    console.info("A:", answer);
  });

  test("Find the top three Elves carrying the most Calories. How many Calories are those Elves carrying in total?", () => {
    const answer = countCaloriesTopThree(inventory);
    console.info("B:", answer);
  });
});
