/**
 * Return Count of elf carrying the max number of calories
 * @param inventory
 */

function countAndSort(inventory: string[]) {
  return inventory
    .map((amount) => parseInt(amount))
    .reduce(
      ({ elf, totals }, amount) => {
        if (isNaN(amount)) {
          elf++;
          totals[elf] = 0;
        } else {
          totals[elf] += amount;
        }
        return {
          elf,
          totals,
        };
      },
      {
        elf: 0,
        totals: [0],
      }
    )
    ["totals"].sort((a, b) => b - a);
}

function countCalories(inventory: Array<string>) {
  return countAndSort(inventory)[0];
}

function countCaloriesTopThree(inventory: string[]) {
  const [one, two, three] = countAndSort(inventory);
  return one + two + three;
}

export { countCalories, countCaloriesTopThree };
