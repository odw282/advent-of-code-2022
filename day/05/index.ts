/**
 * --- Day 5: Supply Stacks ---
 */
import { chunk, flatten, padEnd, take, zip } from "lodash";

export enum CrateMoverType {
  "CM9000",
  "CM9001",
}

function sortCrates(
  list: string[],
  type: CrateMoverType = CrateMoverType.CM9000
) {
  const split = list.indexOf("");
  const stack = list.slice(0, split - 1);
  const width = stack.reduce(
    (max, line) => (line.length > max ? line.length : max),
    0
  );
  const multipleOfFour = width + (4 - (width % 4));
  const crates = zip(
    ...stack.map((crate) =>
      flatten(
        chunk(padEnd(crate, multipleOfFour, " ").split(""), 4).flatMap((line) =>
          line.slice(1, 2)
        )
      )
    )
  ).map((line) => line.filter((char) => char != " "));

  list.slice(split + 1).forEach((move) => {
    const [amount, from, to] = move
      .split(" ")
      .map((i) => parseInt(i))
      .filter((i) => !isNaN(i));

    if (type == CrateMoverType.CM9000) {
      for (let i = 0; i < amount; i++) {
        crates[to - 1].unshift(crates[from - 1].shift());
      }
    } else {
      crates[to - 1].unshift(...crates[from - 1].splice(0, amount));
    }
  });

  return crates.map((stack) => take(stack, 1)).join("");
}

export { sortCrates };
