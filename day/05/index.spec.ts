import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { CrateMoverType, sortCrates } from "./index";

const testInput = [
  "    [D]",
  "[N] [C]",
  "[Z] [M] [P]",
  " 1   2   3",
  "",
  "move 1 from 2 to 1",
  "move 3 from 1 to 3",
  "move 2 from 2 to 1",
  "move 1 from 1 to 2",
];

const amounts = readFileIntoArray(path.join(__dirname, "./input.txt"));

describe("--- Day 5: Supply Stacks ---", () => {
  test("Test Case A", () => {
    const answer = sortCrates(testInput);
    expect(answer).toEqual("CMZ");
  });

  test("Solution A", () => {
    const answer =  sortCrates(amounts);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = sortCrates(testInput, CrateMoverType.CM9001);
    expect(answer).toEqual("MCD");
  });

  it("Solution B", () => {
    const answer = sortCrates(amounts, CrateMoverType.CM9001);
    console.info("B:", answer);
  });
});
