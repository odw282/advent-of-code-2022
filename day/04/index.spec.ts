import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { findOverlaps, findPartialOverlaps } from './index';

const testInput = [
  "2-4,6-8",
  "2-3,4-5",
  "5-7,7-9",
  "2-8,3-7",
  "6-6,4-6",
  "2-6,4-8"
];

const amounts = readFileIntoArray(path.join(__dirname, "./input.txt"));

describe("--- Day 4: Camp Cleanup ---", () => {
  test("Test Case A", () => {
    const answer = findOverlaps(testInput);
    expect(answer).toEqual(2);
  });

  test("Solution A", () => {
    const answer = findOverlaps(amounts)
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = findPartialOverlaps(testInput);
    expect(answer).toEqual(4);
  });

  it("Solution B", () => {
    const answer = findPartialOverlaps(amounts);
    console.info("B:", answer);
  });
});
