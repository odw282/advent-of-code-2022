/**
 * Stub
 */

function findOverlaps(list: string[]) {
  return list.reduce((overlap, pair) => {
    const [[al, ar], [bl, br]] = pair
        .split(",")
        .filter((_) => _ !== ",")
        .map((_) =>
            _.split("-")
                .filter((_) => _ !== "-")
                .map((_) => parseInt(_))
        );
    if ((bl >= al && br <= ar) || (al >= bl && ar <= br)) {
      overlap++;
    }
    return overlap;
  }, 0);
}

function findPartialOverlaps(list: string[]) {
  return list.reduce((overlap, pair) => {
    const [[al, ar], [bl, br]] = pair
        .split(",")
        .filter((_) => _ !== ",")
        .map((_) =>
            _.split("-")
                .filter((_) => _ !== "-")
                .map((_) => parseInt(_))
        );
    if ((al >= bl && al <= br) || (ar >= bl && ar <= br) || (bl >= al && bl <= ar) || (br >= al && br <= ar)) {
      overlap++;
    }

    return overlap;
  }, 0);
}


export { findOverlaps, findPartialOverlaps };
