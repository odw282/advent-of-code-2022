import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { cheat, countScore } from './index';

const testInput = ['A Y',
    'B X',
    'C Z'];

const amounts = readFileIntoArray(path.join(__dirname, "./input.txt"));
describe("Day 2: Rock Paper Scissors", () => {

    test("Test Case A", () => {
        const answer = countScore(testInput);
        expect(answer).toEqual(15);
    });

    test("Solution A", () => {
      const answer = countScore(amounts);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = cheat(testInput)
      expect(answer).toEqual(12);
    });

    it("Following the Elf's instructions for the second column, what would your total score be if everything goes exactly according to your strategy guide?", () => {
      const answer = cheat(amounts);
      console.info("B:", answer);
    });

});
