/**
 * Stub
 */

const scoreTable = {
  X: 1,
  Y: 2,
  Z: 3,
};

function cheat(list: string[]) {
  return list.reduce((score, round) => {
    const [opponent, , action] = round.split("");
    let you = "";

    switch (action) {
      // lose
      case "X":
        switch (opponent) {
          // Rock
          case "A":
            you = "Z";
            break;
          case "B":
            you = "X";
            break;
          case "C":
            you = "Y";
            break;
        }
        break;
      // draw
      case "Y":
        switch (opponent) {
          // Rock
          case "A":
            you = "X";
            break;
          case "B":
            you = "Y";
            break;
          case "C":
            you = "Z";
            break;
        }
        break;
      // win
      case "Z":
        switch (opponent) {
          // Rock
          case "A":
            you = "Y";
            break;
          case "B":
            you = "Z";
            break;
          case "C":
            you = "X";
            break;
        }
        break;
    }

    let p1 = 0,
      p2: number;

    // draw
    if (
      (opponent == "A" && you == "X") ||
      (opponent == "B" && you == "Y") ||
      (opponent == "C" && you == "Z")
    ) {
      p1 = 3;
      // lost
    } else if (
      (opponent == "A" && you == "Z") ||
      (opponent == "B" && you == "X") ||
      (opponent == "C" && you == "Y")
    ) {
      // won
    } else {
      p1 = 6;
    }

    // @ts-ignore
    p2 = scoreTable[you];

    return score + p1 + p2;
  }, 0);
}

function countScore(list: string[]) {
  return list.reduce((score, round) => {
    const [opponent, , you] = round.split("");

    let p1 = 0,
      p2: number;

    // draw
    if (
      (opponent == "A" && you == "X") ||
      (opponent == "B" && you == "Y") ||
      (opponent == "C" && you == "Z")
    ) {
      p1 = 3;
      // lost
    } else if (
      (opponent == "A" && you == "Z") ||
      (opponent == "B" && you == "X") ||
      (opponent == "C" && you == "Y")
    ) {
      // won
    } else {
      p1 = 6;
    }

    // @ts-ignore
    p2 = scoreTable[you];

    return score + p1 + p2;
  }, 0);
}

export { countScore, cheat };
