import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { SIGNAL_TYPE, tune } from './index';

const testInput = ['mjqjpqmgbljsphdztnvjfqwrcgsmlb'];

const list = readFileIntoArray(path.join(__dirname, "./input.txt"));

describe("--- Day 6: Tuning Trouble ---", () => {

    test("Test Case A", () => {
      const answer = tune(testInput);
      expect(answer).toEqual(7);
    });

    test("Solution A", () => {
      const answer = tune(list)
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = tune(testInput, SIGNAL_TYPE.MESSAGE);
      expect(answer).toEqual(19);
    });

    it("Solution B", () => {
      const answer = tune(list, SIGNAL_TYPE.MESSAGE)
      console.info("B:", answer);
    });

});
