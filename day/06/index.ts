/**
 * Stub
 */
import { uniq } from 'lodash';

export enum SIGNAL_TYPE {
  PACKET,
  MESSAGE
}

function tune(messages: string[], type: SIGNAL_TYPE = SIGNAL_TYPE.PACKET) {
  const signal_len = type === SIGNAL_TYPE.PACKET ? 4 : 14;
  const message = messages[0].split('');
  for (let i = signal_len; i < message.length ; i++) {
    if (uniq(message.slice(i - signal_len, i)).length === signal_len) {
      return i;
    }
  }
  return 0;
}

export {
  tune
};
