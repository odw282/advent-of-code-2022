/**
 * sumPriorities
 */

/* prettier-ignore */
const prios = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

function sumPriorities(list: string[]) {
  return list.reduce((total, sack) => {
    const data = sack.split("").map((item) => prios.indexOf(item) + 1);
    const a = data.slice(0, data.length / 2);
    const b = data.slice(data.length / 2);
    const prio = a.find((prioA) => b.find((prioB) => prioA === prioB));
    return total + prio;
  }, 0);
}

function sumBadge(list: string[]) {
  let total = 0;
  for (let i = 0; i < list.length - 2; i += 3) {
    const a = list[i].split("");
    const b = list[i + 1].split("");
    const c = list[i + 2].split("");
    prios.forEach((prio, index) => {
      if (a.includes(prio) && b.includes(prio) && c.includes(prio)) {
        total += index + 1;
      }
    });
  }
  return total;
}

export { sumPriorities, sumBadge };
