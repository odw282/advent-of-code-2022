import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { sumBadge, sumPriorities } from './index';

const testInput = [
    'vJrwpWtwJgWrhcsFMMfFFhFp',
    'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
    'PmmdzqPrVvPwwTWBwg',
    'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
    'ttgJtRGJQctTZtZT',
    'CrZsJsPPZsGzwwsLwLmpwMDw'
];

const amounts = readFileIntoArray(path.join(__dirname, "./input.txt"));

describe("--- Day 3: Rucksack Reorganization ---", () => {

    test("Test Case A", () => {
      const answer = sumPriorities(testInput);
      expect(answer).toEqual(157);
    });

    test("Solution A", () => {
      const answer = sumPriorities(amounts);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = sumBadge(testInput);
      expect(answer).toEqual(70);
    });

    it("Solution B", () => {
      const answer = sumBadge(amounts)
      console.info("B:", answer);
    });

});
